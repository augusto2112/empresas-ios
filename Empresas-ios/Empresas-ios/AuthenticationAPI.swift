//
//  UserAPI.swift
//  iOS Helpers Swift
//
//  Created by Jota Melo on 15/02/17.
//  Copyright © 2017 Jota. All rights reserved.
//

import UIKit


class AuthenticationAPI: APIRequest {

    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
    }
    
    @discardableResult
    static func loginWith(email: String, password: String, callback: ((_ error: API.RequestError?) -> Void)?) -> AuthenticationAPI {
        
        let request = AuthenticationAPI(method: .post, path: "users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            callback?(error)
        }
        request.shouldSaveInCache = false
        
        request.makeRequest()
        return request
    }
}
