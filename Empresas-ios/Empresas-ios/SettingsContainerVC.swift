//
//  SettingsContainerVC.swift
//  Empresas-ios
//
//  Created by Augusto on 5/15/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import UIKit

class SettingsContainerVC: UIViewController {

    @IBAction func backButtonPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

}
