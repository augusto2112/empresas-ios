//
//  Extensions.swift
//  Empresas-ios
//
//  Created by Augusto on 5/13/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func centerText(_ text: String, withColor color: UIColor, withFont font: UIFont) -> UIImage {
        let textFontAttributes = [
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: color,
            ] as [String : Any]
        
        let textSize = NSString(string: text).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: textFontAttributes, context: nil)
        let xCoord = size.width / 2 - textSize.width / 2
        let yCoord = size.height / 2 - textSize.height / 2
        UIGraphicsBeginImageContext(size)

        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let rect = CGRect(x: xCoord, y: yCoord, width: size.width, height: size.height)
        text.draw(in: rect, withAttributes: textFontAttributes)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    static func generateLogo(ofString string: String, withSize size: CGSize, withColor color: UIColor, withFontSize fontSize: CGFloat) -> UIImage {
        var logoName = ""
        if string.characters.count >= 3 {
            // is this really the best way to substring??
            logoName = string.substring(to: string.index(string.startIndex, offsetBy: 3)).uppercased()
        } else {
            logoName = string.uppercased()
        }
        
        let background = UIImage(color: color, size: size)!
        let font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightBold)
        return background.centerText(logoName, withColor: UIColor.white, withFont: font)
    }


}

extension UIColor {
    
    static func from(string1: String, string2: String) -> UIColor {
        var sum = 1
        for (n, c) in string1.unicodeScalars.enumerated() {
            sum += Int(c.value) * (n + 1)
        }
        for (n, c) in string2.unicodeScalars.enumerated() {
            sum += Int(c.value) * (n + 1)
        }
        let red = CGFloat(sum % 255) / 255
        sum *= Int(string1.characters.count)
        let green = CGFloat(sum % 255) / 255
        sum *= Int(string2.characters.count)
        let blue = CGFloat(sum % 255) / 255
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
