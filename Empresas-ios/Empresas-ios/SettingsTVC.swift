//
//  SettingsTVC.swift
//  Empresas-ios
//
//  Created by Augusto on 5/15/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import UIKit


class SettingsTVC: UITableViewController {
    struct Constants {
        static let searchMethod = "searchMethod"
        static let sortMethod = "sortMethod"
    }
    
    fileprivate var searchCell: UITableViewCell?
    fileprivate var sortCell: UITableViewCell?
    
    override func viewDidAppear(_ animated: Bool) {
        let searchMethod = UserDefaults.standard.integer(forKey: Constants.searchMethod)
        searchCell = tableView.cellForRow(at: IndexPath(row: searchMethod, section: 0))
        searchCell?.accessoryType = .checkmark
        
        let sortMethod = UserDefaults.standard.integer(forKey: Constants.sortMethod)
        sortCell = tableView.cellForRow(at: IndexPath(row: sortMethod, section: 1))
        sortCell?.accessoryType = .checkmark

    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            searchCell?.accessoryType = .none
            searchCell = tableView.cellForRow(at: indexPath)
            searchCell?.accessoryType = .checkmark
            UserDefaults.standard.set(indexPath.row, forKey: Constants.searchMethod)
        case 1:
            sortCell?.accessoryType = .none
            sortCell = tableView.cellForRow(at: indexPath)
            sortCell?.accessoryType = .checkmark
            UserDefaults.standard.set(indexPath.row, forKey: Constants.sortMethod)

        default:
            break
        }
    }
}
