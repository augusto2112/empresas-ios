//
//  Company.swift
//  Empresas-ios
//
//  Created by Augusto on 5/13/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import Foundation
import UIKit

struct Company : Mappable {
    let id: Int
    let enterpriseName: String
    let description: String
    let city: String
    let country: String
    let typeId: Int
    let typeName: String
    var logo: UIImage?
    
    init(mapper: Mapper) {
        id = mapper.keyPath("id")
        enterpriseName = mapper.keyPath("enterprise_name")
        description = mapper.keyPath("description")
        city = mapper.keyPath("city")
        country = mapper.keyPath("country")
        typeId = mapper.keyPath("enterprise_type.id")
        typeName = mapper.keyPath("enterprise_type.enterprise_type_name")
    }

}
