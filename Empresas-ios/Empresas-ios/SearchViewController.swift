//
//  SearchViewController.swift
//  Empresas-ios
//
//  Created by Augusto on 5/11/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var logo: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var texto: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    fileprivate var companies = [(category:String, companiesInCategory:[Company])]()
    fileprivate var selectedCompany: Company?
    
    fileprivate enum searchOptions: Int {
        case byName
        case byCategory
    }
    fileprivate var selectedSearch = searchOptions.byName {
        didSet {
            if oldValue != selectedSearch {
                if selectedSearch == .byName {
                    searchBar.keyboardType = .default
                    searchBar.inputAccessoryView = nil
                } else {
                    searchBar.keyboardType = .numberPad
                    searchBar.inputAccessoryView = keyboardToolbar
                }
            }
        }
    }
    
    fileprivate lazy var keyboardToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(SearchViewController.dismissKeyboard))
        toolbar.items = [flexBarButton, doneBarButton]
        toolbar.tintColor = UIColor.tiDarkishPink
        return toolbar
    }()
    
    func dismissKeyboard() {
        searchBarSearchButtonClicked(searchBar)
    }

    fileprivate enum sortOptions: Int {
        case alphabetically
        case byCategory
    }
    fileprivate var selectedSort = sortOptions.alphabetically {
        didSet {
            if !tableView.isHidden && oldValue != selectedSort {
                rearrange()
            }
        }
    }
    
    
    // MARK: View controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedSearch = SearchViewController.searchOptions(rawValue: UserDefaults.standard.integer(forKey: SettingsTVC.Constants.searchMethod))!
        selectedSort = SearchViewController.sortOptions(rawValue: UserDefaults.standard.integer(forKey: SettingsTVC.Constants.sortMethod))!
    }
    
    @IBAction func searchClicked(_ sender: UIBarButtonItem) {
        if searchBar.isHidden {
            searchBar.isHidden = false
            searchBar.text = ""
            sender.title = "Cancel"
            sender.image = nil
            searchBar.becomeFirstResponder()
            let searchBarWidth = searchBar.bounds.width
            searchBar.bounds = CGRect(x: 0, y: 0, width: 0, height: searchBar.bounds.height)
            UIView.animate(withDuration: 0.4) {
                self.searchBar.bounds = CGRect(x: 0, y: 0, width: searchBarWidth, height: self.searchBar.bounds.height)
            }
        } else {
            searchBar.resignFirstResponder()
            sender.title = nil
            sender.image = #imageLiteral(resourceName: "icSearchCopy")
            let searchBarWidth = searchBar.bounds.width
            texto.isHidden = false

            UIView.animate(withDuration: 0.4, animations: {
                self.searchBar.bounds = CGRect(x: 0, y: 0, width: 0, height: self.searchBar.bounds.height)
            }, completion: { (success) in
                self.searchBar.bounds = CGRect(x: 0, y: 0, width: searchBarWidth, height: self.searchBar.bounds.height)
                self.searchBar.isHidden = true
            })
            tableView.isHidden = true
            texto.isHidden = false
        }
    }
    
    
    // MARK: - SearchBar Delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        spinner.isHidden = false
        spinner.startAnimating()

        DispatchQueue.global(qos: .userInitiated).async { [weakSelf = self] in
            var name: String?
            var type: String?
            if weakSelf.selectedSearch == searchOptions.byName{
                name = searchBar.text
            } else if weakSelf.selectedSearch == searchOptions.byCategory {
                type = searchBar.text
            }
            SearchAPI.search(type: type, name: name) { (companies, error, cache) in
                if error == nil && companies != nil {
                    if companies!.count > 0 {
                        weakSelf.divideInCategories(comps: companies!)
                        weakSelf.generateLogos()
                        DispatchQueue.main.async {
                            weakSelf.tableView.reloadData()
                            weakSelf.tableView.isHidden = false
                            weakSelf.texto.isHidden = true
                            weakSelf.spinner.isHidden = true
                            weakSelf.spinner.stopAnimating()

                        }
                    } else {
                        let alert = UIAlertController(title: "Nenhuma empresa encontrada", message: "Nenhuma empresa com este nome ou código foi encontrada!", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alert.addAction(ok)
                        DispatchQueue.main.async {
                            weakSelf.present(alert, animated: true, completion: nil)
                            weakSelf.spinner.isHidden = true
                            weakSelf.spinner.stopAnimating()
                            
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func rearrange() {
        let comps = companies.flatMap { $1 }
        divideInCategories(comps: comps)
        tableView.reloadData()
    }
        
    fileprivate func divideInCategories(comps: [Company]) {
        self.companies.removeAll()
        
        let companies = comps.sorted { (c1, c2) -> Bool in
            c1.enterpriseName < c2.enterpriseName
        }
        var headerTitle: (Company) -> String
        
        if selectedSort == .alphabetically {
            headerTitle = { (company: Company) -> String in
                String(company.enterpriseName.characters.first!).uppercased()
            }
        } else {
            headerTitle = { (company: Company) -> String in
                company.typeName
            }
        }
        
        for company in companies {
            let header = headerTitle(company)
            if let index = self.companies.index(where: { (category: String, _) -> Bool in
                category == header
            }) {
                self.companies[index].companiesInCategory.append(company)
            } else {
                self.companies.append((header, [company]))
            }
        }
        self.companies.sort(by: { (c1, c2) -> Bool in
            c1.category < c2.category
        })

    }
    
    fileprivate func generateLogos() {
        for i in 0..<companies.count { // for..in doesnt work for mutating structs
            for j in 0..<companies[i].companiesInCategory.count {
                companies[i].companiesInCategory[j].logo = UIImage.generateLogo(
                    ofString: companies[i].companiesInCategory[j].enterpriseName,
                    withSize: CGSize(width: 90, height: 69),
                    withColor: UIColor.from(string1: companies[i].companiesInCategory[j].enterpriseName,
                                            string2: companies[i].companiesInCategory[j].description),
                    withFontSize: 33)
            }

        }
        
    }
    
    // MARK: - TableView Data Source
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath) as! CompanyTableViewCell
        let company = companies[indexPath.section].companiesInCategory[indexPath.row]
        cell.companyView.image = company.logo
        cell.name.text = company.enterpriseName
        cell.type.text = company.typeName
        cell.country.text = company.city + ", " + company.country
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return companies[section].category
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies[section].companiesInCategory.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return companies.count
    }
    
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }
    
    fileprivate let segueIdenfitier = "showDetail"
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCompany = companies[indexPath.section].companiesInCategory[indexPath.row]
        performSegue(withIdentifier: segueIdenfitier, sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailViewController, let selectedCompany = selectedCompany {
            detailVC.tempDescription = selectedCompany.description
            detailVC.tempTitle = selectedCompany.enterpriseName
        }
    }
    
    @IBAction func unwindToSearch(segue: UIStoryboardSegue) {
        if segue.source is DetailViewController {
            searchBar.text = ""
            searchBar.isHidden = true
            tableView.isHidden = true
            texto.isHidden = false
            searchClicked(searchButton)
        }

    }

    
    
}



























