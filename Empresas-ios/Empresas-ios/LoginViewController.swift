//
//  LoginViewController.swift
//  Empresas-ios
//
//  Created by Augusto on 5/11/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    
    fileprivate struct Constants {
        static let login = "login"
    }


    @IBAction func loginPressed() {
        spinner.isHidden = false
        spinner.startAnimating()
        
        DispatchQueue.global(qos: .userInitiated).async { [weakSelf = self] in
            AuthenticationAPI.loginWith(email: weakSelf.emailTextField.text!, password: weakSelf.passwordTextField.text!) {(error) in
                DispatchQueue.main.async {
                    weakSelf.spinner.isHidden = true
                    weakSelf.spinner.stopAnimating()
                    if error == nil {
                        weakSelf.performSegue(withIdentifier: Constants.login, sender: weakSelf)
                    }

                }
            }

        }

    }
    
    // MARK: - Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            loginPressed()
            textField.resignFirstResponder()
        }
        return true
    }

}
