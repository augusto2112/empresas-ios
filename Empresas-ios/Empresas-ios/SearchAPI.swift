//
//  SearchAPI.swift
//  Empresas-ios
//
//  Created by Augusto on 5/11/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

class SearchAPI: APIRequest {
    
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
    }
    
    @discardableResult
    static func search(type: String?, name: String?, callback: ResponseBlock<[Company]>?) -> SearchAPI {
        var urlParamenters = [String : Any]()
        if let type = type {
            urlParamenters["enterprise_types"] = type
        }
        if let name = name {
            urlParamenters["name"] = name
        }
        let request = SearchAPI(method: .get, path: "enterprises", parameters: nil, urlParameters: urlParamenters, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any?] {
                var companies = [Company]()
                if let enterprises  = response["enterprises"] as? [[String: Any]] {
                    for enterprise in enterprises {
                        companies.append(Company(dictionary: enterprise))
                    }
                    callback?(companies, nil, cache)
                }
            }
        }
        
        request.makeRequest()
        return request
    }
}
