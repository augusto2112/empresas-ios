//
//  DetailViewController.swift
//  Empresas-ios
//
//  Created by Augusto on 5/13/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleNavigationItem: UINavigationItem!
    
    var tempDescription: String?
    var tempTitle: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tempDescription = tempDescription {
            descriptionTextView.text = tempDescription
        }
        if let tempTitle = tempTitle {
            titleNavigationItem.title = tempTitle
        }
        
        if let tempDescription = tempDescription, let tempTitle = tempTitle {
           logoImageView.image =
            UIImage.generateLogo(
                ofString: tempTitle,
                withSize: CGSize(width: 375, height: 184),
                withColor: UIColor.from(string1: tempTitle, string2: tempDescription),
                withFontSize: 130)
        }
    }

    @IBAction func backPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
            


}
