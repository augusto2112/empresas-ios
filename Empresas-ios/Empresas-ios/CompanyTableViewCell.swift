//
//  CompanyTableViewCell.swift
//  Empresas-ios
//
//  Created by Augusto on 5/12/17.
//  Copyright © 2017 Nihil Games. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    @IBOutlet weak var companyView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var country: UILabel!
}
